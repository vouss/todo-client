var app = angular.module('todoService', ['xeditable']);

app.run(function(editableOptions) {
    editableOptions.theme = 'bs3';
});

app.controller("MainTaskController", function ($scope, $http) {
    var getTasks = function () {
        $http.get('http://localhost:8765/api/task')
            .success(function (data) {
                $scope.tasks = data;
            })
            .error(function () {
                $scope.errorStat = true;
                $scope.text = 'Connection refused. It\'s something wrong with APItodo!';
            });
    };

    getTasks();

    $scope.statusAdd = true;
    $scope.errorStat = false;


    $scope.priorities = [//TODO Wyciągać te wartości z bazy!
        {
            name : 'higher', power : 1
        },         {
            name : 'high', power : 2
        },         {
            name : 'medium', power : 3
        },         {
            name : 'low', power : 4
        },         {
            name : 'lowest', power : 5
        }
    ];

    $scope.add = function () {
        //console.log( {title:this.taskTitle, status_id:parseInt(this.taskStatus),priority_power:this.cPriority.power,description : 'trollololo'});
        $http.post('http://localhost:8765/api/task', {title:this.taskTitle, status_id:parseInt(this.taskStatus),priority_power:this.cPriority.power,description : 'trollololo'})
            .success(function () {
                console.log('dodano');
                $scope.toggle();
                getTasks();
            })
            .error(function (data) {
                console.log(data);
            });
    };

    $scope.updateTask = function(data,task_id,status_change) {
        //console.log(data.title + ' ' + data.power + ' ' + status_change);

        if(status_change!=9) {
            return $http.put('http://localhost:8765/api/task/' + task_id, {
                status_change   : status_change
            }).success(function() {
                getTasks();
            });
        }

        return $http.put('http://localhost:8765/api/task/' + task_id, {
                title           : data.title,
                priority_power  : data.power,
                status_change   : status_change
        }).success(function() {
                getTasks();
        });
    };

    $scope.removeTask = function(task_id) {
        $http.delete('http://localhost:8765/api/task/' + task_id)
            .success(function () {
                getTasks();
            })
            .error(function (data) {
                console.log(data);
            });
    };

    $scope.toggle = function() {
        $scope.addTaskForm.$setPristine(); //TODO Resetowanie pól w formularzu
        $scope.statusAdd = !$scope.statusAdd;
    };

    $scope.todo = function (task) {
        return task.status.text == "todo";
    };
    $scope.pending = function (task) {
        return task.status.text == "pending";
    };
    $scope.done = function (task) {
        return task.status.text == "done";
};
});